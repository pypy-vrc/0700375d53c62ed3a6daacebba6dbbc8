const blonix = (sName, bMaster = false) => {
  const now = new Date();

  // blonix needs to be run at 06:00 ~ 06:59 KST
  const kst = new Date(now.getTime() + (now.getTimezoneOffset() + 540) * 60000);

  // if (kst.getHours() !== 6) {
  //   return "----";
  // }

  // bypass :p
  kst.setHours(6);

  // generate attendance token
  let code = 11;

  for (const s of sName.toLowerCase().split("")) {
    code += /[a-z]/.test(s) && s.charCodeAt(0) - 96;
  }

  code *= sName.length;
  code *= kst.getMonth() + 21;
  code *= kst.getDate();
  code *= (kst.getDate() % 2) + 1;
  code *= (kst.getDate() % 3) + 1;

  // orderly officer flag
  if (bMaster) {
    code += 1234;
  }

  code %= 10000;

  return `${code}`;
};

console.log(blonix("pypy", true));
